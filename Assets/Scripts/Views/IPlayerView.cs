﻿using System;

public interface IPlayerView
{
	void PrepareForGame();
	void WaitInteractiveChoice(Action<IRoundChoice> onInteractiveChooseDone);
	void ShowChoice();
}