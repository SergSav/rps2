﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class HumanPlayerView : MonoBehaviour, IPlayerView
{
	[SerializeField]
	private GameObject _choiceButtonPrefab;
	[SerializeField]
	private SpriteAtlas _imageAtlas;

	private List<GameObject> _playerButtonList;
	private IRoundChoice _currentPlayerChoice;
	private Action<IRoundChoice> _onInteractiveChooseDone;

	public void PrepareForGame()
    {
		var choiceList = new List<IRoundChoice>
		{
			new RockChoice(),
			new PaperChoice(),
			new ScissorsChoice()
		};
		_playerButtonList = new List<GameObject>(choiceList.Count);
		foreach (BaseChoiceData data in choiceList)
		{
			_playerButtonList.Add(CreateButton(data));
		}
	}

	private GameObject CreateButton(IRoundChoice data)
	{
		GameObject button = Instantiate(_choiceButtonPrefab, this.transform);
		Image img = button.GetComponent<Image>();
		img.sprite = _imageAtlas.GetSprite(data.ImageName);
		var controller = button.GetComponent<ButtonController>();
		controller.onClickCallback = OnChoiceButtonClick;
		controller.buttonData = data;
		controller.buttonView = button;
		return button;
	}

	private void OnChoiceButtonClick(IRoundChoice data, GameObject view)
	{
		if (_currentPlayerChoice != null)
			return;
		_currentPlayerChoice = data;
		LockButtons(view);

		_onInteractiveChooseDone.Invoke(data);
	}

	private void LockButtons(GameObject exceptButton)
	{
		foreach (GameObject button in _playerButtonList)
		{
			if (button == exceptButton)
				continue;
			button.GetComponent<Button>().interactable = false;
		}
	}

	private void UnlockButtons()
	{
		foreach (GameObject button in _playerButtonList)
			button.GetComponent<Button>().interactable = true;
	}

	private void ResetForNextRound()
	{
		_currentPlayerChoice = null;
	}

	public void WaitInteractiveChoice(Action<IRoundChoice> onInteractiveChooseDone)
	{
		ResetForNextRound();
		_onInteractiveChooseDone = onInteractiveChooseDone;
		UnlockButtons();
	}

	public void ShowChoice()
	{
		// immediately show
	}
}
