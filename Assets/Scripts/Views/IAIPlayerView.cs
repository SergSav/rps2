﻿using RPS2.Assets.Scripts;

public interface IAIPlayerView : IPlayerView
{
	void SetBrain(IAIBrain brain);
}