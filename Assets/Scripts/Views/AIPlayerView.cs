﻿using System;
using System.Collections;
using System.Collections.Generic;
using RPS2.Assets.Scripts;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class AIPlayerView : MonoBehaviour, IAIPlayerView
{
	[SerializeField]
	private GameObject _choiceButtonPrefab;
	[SerializeField]
	private SpriteAtlas _imageAtlas;

	private List<GameObject> _playerButtonList;
	private Action<IRoundChoice> _interactiveChoice;
	private IAIBrain _brain;
	private IRoundChoice _currentChoice;

	public void PrepareForGame()
	{
		var choiceList = new List<IRoundChoice>
		{
			new RockChoice(),
			new PaperChoice(),
			new ScissorsChoice()
		};
		_playerButtonList = new List<GameObject>(choiceList.Count);
		foreach (BaseChoiceData data in choiceList)
		{
			_playerButtonList.Add(CreateButton(data));
		}
	}

	public void SetBrain(IAIBrain brain)
	{
		_brain = brain;
	}

	private GameObject CreateButton(IRoundChoice data)
	{
		GameObject button = Instantiate(_choiceButtonPrefab, this.transform);
		Image img = button.GetComponent<Image>();
		img.sprite = _imageAtlas.GetSprite(data.ImageName);
		var controller = button.GetComponent<ButtonController>();
		controller.buttonData = data;
		controller.buttonView = button;
		return button;
	}

	private void LockButtons(IRoundChoice choice)
	{
		foreach (GameObject button in _playerButtonList)
		{
			if (button.GetComponent<ButtonController>().buttonData.Type == choice.Type)
				continue;
			button.GetComponent<Button>().interactable = false;
		}
	}

	private void UnlockButtons()
	{
		foreach (GameObject button in _playerButtonList)
			button.GetComponent<Button>().interactable = true;
	}

	public void WaitInteractiveChoice(Action<IRoundChoice> onInteractiveChooseDone)
	{
		_interactiveChoice = onInteractiveChooseDone;
		UnlockButtons();

		StartCoroutine(AIMakeChoice());
	}

	private IEnumerator AIMakeChoice()
	{
		_currentChoice = _brain.MakeDecision();
		yield return new WaitForSeconds(UnityEngine.Random.Range(0.5f, 1.5f));
		_interactiveChoice.Invoke(_currentChoice);
	}

	public void ShowChoice()
	{
		LockButtons(_currentChoice);
	}
}
