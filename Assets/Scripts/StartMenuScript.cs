﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenuScript : MonoBehaviour
{
    public void OnNewGameButtonClick()
	{
		SceneManager.LoadScene("Gameplay");
	}

	public void OnAISettingsButtonClick()
	{
		// TODO: open setting window
	}

	public void OnExitButtonClick()
	{
		Application.Quit();
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
	}
}
