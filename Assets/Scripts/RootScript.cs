﻿using UnityEngine;
using UnityEngine.SceneManagement;

class RootScript : MonoBehaviour
{
	private void OnEnable()
	{
		SceneManager.LoadScene("StartMenu");
	}
}
