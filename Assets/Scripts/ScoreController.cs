﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    [SerializeField]
    private Text _scoreText;

    void Start()
    {
        UpdateValue("0:0");
	}

	public void UpdateValue(string text)
	{
		_scoreText.text = text;
	}
}
