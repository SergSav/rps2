﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "settings/GameSettings")]
public class GameSettings : ScriptableObject
{
    public bool UseProbabilityAI;
    public float AIWinProbability;
}