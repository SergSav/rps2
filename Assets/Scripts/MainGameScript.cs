﻿using RPS2.Assets.Scripts;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MainGameScript : MonoBehaviour
{
	#region variables
    [SerializeField]
    private GameObject _resultText;
	[SerializeField]
	private GameObject leftPanel;
	[SerializeField]
	private GameObject rightPanel;
	[SerializeField]
	private GameObject scorePanel;
	[SerializeField]
	private GameSettingsWithPlayers _gameSettings;
	private GameManager _mainLogic;
    private bool _needReset = true;
	#endregion

	void Start()
    {
		_mainLogic = new GameManager(_gameSettings);
		_mainLogic.SetViewForPlayers(new[] { leftPanel.GetComponent<IPlayerView>(), rightPanel.GetComponent<IPlayerView>() });
		_mainLogic.StartGame(onRoundEnd);
		ResetForNextRound();
	}

	private void onRoundEnd()
	{
		StartCoroutine(ShowResult());
	}
	
    public void ResetForNextRound()
    {
		if (!_needReset)
			return;
		_resultText.SetActive(false);
		_mainLogic.ProcessNextRound();
		_needReset = false;
	}

	IEnumerator ShowResult()
    {
        PrepareResultText();
        yield return new WaitForSeconds(.2f);
        _resultText.SetActive(true);

        yield return new WaitForSeconds(.2f);
		UpdateScore();

		_needReset = true;
    }

	private void UpdateScore()
	{
		var score = _mainLogic.GetCurrentScore();
		scorePanel.GetComponent<ScoreController>().UpdateValue(score);
	}

	private void PrepareResultText()
    {
        Text result = _resultText.GetComponent<Text>();
        result.color = Color.green;
        var text = "";
        var color = Color.gray;
		switch (_mainLogic.GameState.RoundResult)
		{
			case RoundResult.DRAW:
				text = "DRAW";
				break;
			case RoundResult.HAS_WINNER:
				text = _mainLogic.GameState.Winner.IsHuman ? "YOU WIN!" : "YOU LOSE";
				color = _mainLogic.GameState.Winner.IsHuman ? Color.yellow : Color.red;
				break;
		}
        result.text = text;
        result.color = color;
    }

    private void Update()
    {
        if (_needReset && Input.GetKey(KeyCode.Mouse0))
            ResetForNextRound();
        if (Input.GetKey(KeyCode.Escape))
            Application.Quit();
    }
}
