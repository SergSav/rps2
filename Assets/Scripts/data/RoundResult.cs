﻿public enum RoundResult
{
	IDLE,
	DRAW,
	HAS_WINNER
}