﻿public interface IRoundChoice
{
	ChoiceType Type { get; }
	string ImageName { get; }

	bool TestWeakness(IRoundChoice opponentData);
	bool TestStrength(IRoundChoice opponentData);
}