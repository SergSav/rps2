﻿using System.Collections.Generic;

public partial class BaseChoiceData : IRoundChoice
{
    public List<ChoiceType> WeaknessList = new List<ChoiceType>();
    public List<ChoiceType> StrengthList = new List<ChoiceType>();

    public string ImageName { get; protected set; }

    public ChoiceType Type { get; protected set; }

    public bool TestWeakness(IRoundChoice opponentData)
    {
        foreach (ChoiceType type in WeaknessList)
        {
            if (type == opponentData.Type)
                return true;
        }
        return false;
    }

    public bool TestStrength(IRoundChoice opponentData)
    {
        foreach (ChoiceType type in StrengthList)
        {
            if (type == opponentData.Type)
                return true;
        }
        return false;
    }
}