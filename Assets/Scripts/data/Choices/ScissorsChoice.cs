﻿public class ScissorsChoice : BaseChoiceData
{
    public ScissorsChoice()
    {
        WeaknessList.Add(ChoiceType.Rock);
        StrengthList.Add(ChoiceType.Paper);
        ImageName = "scissorsImage";
        Type = ChoiceType.Scissors;
    }
}