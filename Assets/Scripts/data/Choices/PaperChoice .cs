﻿public class PaperChoice : BaseChoiceData
{
    public PaperChoice()
    {
        WeaknessList.Add(ChoiceType.Scissors);
        StrengthList.Add(ChoiceType.Rock);
        ImageName = "paperImage";
        Type = ChoiceType.Paper;
    }
}