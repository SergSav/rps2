﻿public class RockChoice : BaseChoiceData
{
    public RockChoice()
    {
        WeaknessList.Add(ChoiceType.Paper);
        StrengthList.Add(ChoiceType.Scissors);
        ImageName = "rockImage";
        Type = ChoiceType.Rock;
    }
}