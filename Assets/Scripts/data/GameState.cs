﻿using RPS2.Assets.Scripts;
using System.Collections.Generic;
using System.Linq;

public class GameState
{
	private List<IPlayer> _players;
	private Dictionary<int, IRoundChoice> _playersRoundChoices;
	private Dictionary<int, int> _playersScore;
	private RoundResult _roundResult;
	private int _winnerId = -1;

	public RoundResult RoundResult => _roundResult;
	public IPlayer Winner => _players.FirstOrDefault(p => p.getId == _winnerId);
	public Dictionary<int, int> PlayersScore => _playersScore;

	public void SetupPlayers(List<IPlayer> players)
	{
		_players = players;
		_playersRoundChoices = new Dictionary<int, IRoundChoice>(players.Count);
		_playersScore = new Dictionary<int, int>(players.Count);

		foreach (var player in players)
		{
			_playersRoundChoices[player.getId] = null;
			_playersScore[player.getId] = 0;
		}
	}

	public void ResetRoundInfo()
	{
		_winnerId = -1;
		_roundResult = RoundResult.IDLE;
	}

	public void SetPlayerChoice(int id, IRoundChoice choice)
	{
		_playersRoundChoices[id] = choice;
	}

	// TODO: replace to external, refactor it
	public void ProcessRoundEnd()
	{
		foreach (var pair in _playersRoundChoices)
		{
			foreach (var otherPair in _playersRoundChoices)
			{
				if (pair.Key == otherPair.Key)
					continue;
				if (pair.Value.TestStrength(otherPair.Value))
				{
					_winnerId = pair.Key;
				}
				if (pair.Value.TestWeakness(otherPair.Value))
				{
					_winnerId = otherPair.Key;
				}
			}
		}

		_roundResult = _winnerId != -1 ? RoundResult.HAS_WINNER : RoundResult.DRAW;

		if (_winnerId >= 0)
			_playersScore[_winnerId]++;
	}
}
