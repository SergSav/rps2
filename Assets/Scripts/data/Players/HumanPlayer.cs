﻿using System;
using RPS2.Assets.Scripts;

public class HumanPlayer : IPlayer
{
	private int _id;
	private string _name;
	private IPlayerView _view;

	public HumanPlayer(int id, string name)
	{
		_id = id;
		_name = name;
	}

	public int getId => _id;

	public bool IsHuman => true;

	public void MakeChoice(Action<int, IRoundChoice> onChooseDone)
	{
		_view.WaitInteractiveChoice((IRoundChoice choice) => onChooseDone.Invoke(_id, choice));
	}

	public void SetView(IPlayerView view)
	{
		_view = view;
		_view.PrepareForGame();
	}

	public void ShowChoice()
	{
		_view.ShowChoice();
	}
}
