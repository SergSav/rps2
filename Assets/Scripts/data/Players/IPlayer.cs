﻿using System;

namespace RPS2.Assets.Scripts
{
	public interface IPlayer
	{
		int getId { get; }

		void MakeChoice(Action<int, IRoundChoice> onChooseDone);
		void SetView(IPlayerView view);
		void ShowChoice();

		bool IsHuman { get; }
	}
}
