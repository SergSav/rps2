﻿using RPS2.Assets.Scripts;
using System;

public class AIPlayer : IPlayer
{
	private int _id;
	private IAIBrain _brain;
	private string _name;
	private IPlayerView _view;

	public AIPlayer(int id, IAIBrain brain, string name)
	{
		_id = id;
		_brain = brain;
		_name = name;
	}

	public int getId => _id;

	public bool IsHuman => false;

	public void MakeChoice(Action<int, IRoundChoice> onChooseDone)
	{
		_view.WaitInteractiveChoice((IRoundChoice choice) => onChooseDone.Invoke(_id, choice));
	}

	public void SetView(IPlayerView view)
	{
		_view = view;
		_view.PrepareForGame();
		(_view as IAIPlayerView).SetBrain(_brain);
	}

	public void ShowChoice()
	{
		_view.ShowChoice();
	}
}
