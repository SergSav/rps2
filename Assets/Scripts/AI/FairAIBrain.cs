﻿using RPS2.Assets.Scripts;
using System.Collections.Generic;

public class FairAIBrain : IAIBrain
{
	public IRoundChoice MakeDecision()
	{
		var choiceList = new List<IRoundChoice>()
		{
			new PaperChoice(),
			new RockChoice(),
			new ScissorsChoice()
		};
		return choiceList[UnityEngine.Random.Range(0, choiceList.Count)];
	}
}