﻿namespace RPS2.Assets.Scripts
{
	public interface IAIBrain
	{
		IRoundChoice MakeDecision();
	}
}
