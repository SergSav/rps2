﻿using System;
using RPS2.Assets.Scripts;

public class AIBrainFactory
{
	static public IAIBrain GetAIBrain(PlayerType type, float winProbability, GameManager gameManager)
	{
		switch (type)
		{
			case PlayerType.FairAI:
				return new FairAIBrain();
			case PlayerType.ProbabilityAI:
				return new ProbabilityAIBrain(winProbability, gameManager);
			case PlayerType.MemoryAI:
				return new MemoryAIBrain(gameManager);
			default:
				throw new NotSupportedException($"Type {type} not supported");
		}
	}
}
