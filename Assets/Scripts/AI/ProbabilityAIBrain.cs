﻿using RPS2.Assets.Scripts;
using System.Collections.Generic;

public class ProbabilityAIBrain : IAIBrain
{
    private float _winProbability;
	private GameManager _gameManager;

	public ProbabilityAIBrain(float winProbability, GameManager gameManager)
    {
        _winProbability = winProbability;
		_gameManager = gameManager;
	}

	public IRoundChoice MakeDecision()
	{
		return new PaperChoice(); // TODO: valid logic
		//var choiceList = new List<IRoundChoice>()
		//{
		//	new PaperChoice(),
		//	new RockChoice(),
		//	new ScissorsChoice()
		//};

		//var needWin = UnityEngine.Random.value <= _winProbability;
		//foreach (BaseChoiceData data in choiceList)
		//{
		//	if (needWin)
		//	{
		//		if (data.TestStrength(playerChoice))
		//			return data;
		//	}
		//	else
		//	{
		//		if (data.TestWeakness(playerChoice))
		//			return data;
		//	}
		//}
		//return playerChoice;
	}
}