﻿using RPS2.Assets.Scripts;
using System.Collections.Generic;

public class MemoryAIBrain : IAIBrain
{
    private List<IRoundChoice> _opponentChoices;

    public MemoryAIBrain(GameManager gameManager)
    {
		_opponentChoices = new List<IRoundChoice>();
    }

	public IRoundChoice MakeDecision()
	{
		return new PaperChoice(); // TODO: make valid
		/*
		var needWin = Random.value <= _winProbability;
		foreach (BaseChoiceData data in choiceList)
		{
			if (needWin)
			{
				if (data.TestStrength(playerChoice))
					return data;
			}
			else
			{
				if (data.TestWeakness(playerChoice))
					return data;
			}
		}
		return playerChoice;
		*/
	}
}