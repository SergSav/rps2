﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettingsWithPlayers", menuName = "settings/GameSettingsWithPlayers")]
public class GameSettingsWithPlayers : ScriptableObject
{
	public List<PlayerSettings> PlayerSettings;
}

[System.Serializable]
public class PlayerSettings
{
	public string Name;
	public PlayerType Type;
	public float AIWinProbability;
}

public enum PlayerType
{
	Human,
	FairAI,
	ProbabilityAI,
	MemoryAI
}