﻿using System;
using System.Collections.Generic;

namespace RPS2.Assets.Scripts
{
	public class GameManager
	{
		private GameSettingsWithPlayers _settings;
		private List<IPlayer> _players;
		private GameState _gameState;
		private int _choicesCounter;
		private Action _onRoundEnd;

		public GameState GameState => _gameState;

		public GameManager(GameSettingsWithPlayers settings)
		{
			_settings = settings;

			CreatePlayers();
			CreateGameState();
		}

		#region INIT GAME
		private void CreateGameState()
		{
			_gameState = new GameState();
			_gameState.SetupPlayers(_players);
		}

		private void CreatePlayers()
		{
			_players = new List<IPlayer>();
			for (var i = 0; i < _settings.PlayerSettings.Count; i++)
			{
				var player = CreatePlayer(i, _settings.PlayerSettings[i]);
				_players.Add(player);
			}
		}

		private IPlayer CreatePlayer(int id, PlayerSettings playerSettings)
		{
			if (playerSettings.Type == PlayerType.Human)
				return CreateHumanPlayer(id, playerSettings);
			return CreateAIPlayer(id, playerSettings);
		}

		private IPlayer CreateHumanPlayer(int id, PlayerSettings playerSettings)
		{
			return new HumanPlayer(id, playerSettings.Name);
		}

		private IPlayer CreateAIPlayer(int id, PlayerSettings playerSettings)
		{
			var brain = CreateAIBrain(playerSettings.Type, playerSettings.AIWinProbability);
			return new AIPlayer(id, brain, playerSettings.Name);
		}

		public string GetCurrentScore()
		{
			var scoreResult = String.Empty;
			foreach (var pair in _gameState.PlayersScore)
			{
				scoreResult += $"{pair.Value}:";
			}
			return scoreResult.Remove(scoreResult.Length - 1);
		}

		public void SetViewForPlayers(IPlayerView[] playerView)
		{
			if (playerView.Length > _players.Count)
				throw new Exception($"Not valid players count! Views = {playerView.Length}, players = {_players.Count}");

			for (var i = 0; i < playerView.Length; i++)
			{
				_players[i].SetView(playerView[i]);
			}
		}

		private IAIBrain CreateAIBrain(PlayerType type, float AIWinProbability)
		{
			return AIBrainFactory.GetAIBrain(type, AIWinProbability, this);
		}

		#endregion

		public void StartGame(Action onRoundEnd)
		{
			_onRoundEnd = onRoundEnd;
		}

		public void ProcessNextRound()
		{
			_gameState.ResetRoundInfo();
			_choicesCounter = 0;
			foreach (var player in _players)
			{
				player.MakeChoice(OnChooseDone);
			}
		}

		private void OnChooseDone(int id, IRoundChoice choice)
		{
			_gameState.SetPlayerChoice(id, choice);
			_choicesCounter++;
			if (_choicesCounter == _players.Count)
			{
				ProcessRoundEnd();
			}
		}

		private void ProcessRoundEnd()
		{
			ShowPlayerChoices();

			_gameState.ProcessRoundEnd();
			UnityEngine.Debug.Log($"ProcessRoundEnd {_gameState.RoundResult} - {_gameState.Winner}");
			_onRoundEnd.Invoke();
		}

		private void ShowPlayerChoices()
		{
			foreach (var player in _players)
			{
				player.ShowChoice();
			}
		}
	}
}
