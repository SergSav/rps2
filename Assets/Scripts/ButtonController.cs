﻿using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public IRoundChoice buttonData;
    public GameObject buttonView;

    public delegate void ButtonClickCallback(IRoundChoice data, GameObject view);
    public ButtonClickCallback onClickCallback;
    
    public void OnClickEvent()
    {
		onClickCallback?.Invoke(buttonData, buttonView);
	}
}
