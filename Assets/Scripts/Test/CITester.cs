﻿using UnityEngine;

public static class CITester
{
	public static void RunMainTest()
	{
		Debug.Log("<< TEST RUN STARTED...");
		for (var i = 0; i < 100; i++)
		{
			Debug.Log($"fake test #{i} passed - OK");
		}
		Debug.Log(">> TEST RUN FINISHED");
	}
}
